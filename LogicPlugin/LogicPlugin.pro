
TARGET = LogicPlugin
TEMPLATE = lib
QT += core

include(../../plugin.pri)

include(../../Interfaces/Architecture/PluginBase/PluginBase.pri)


HEADERS += \
	plugin.h \
	logicplugin.h

SOURCES += \
	plugin.cpp \
	logicplugin.cpp

DISTFILES += PluginMeta.json

