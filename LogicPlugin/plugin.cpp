#include "plugin.h"

Plugin::Plugin() :
	QObject(nullptr),
	PluginBase(this)
  , m_impl(new LogicPlugin(this))
{
	initPluginBase(
				{
					{INTERFACE(IPlugin), this}
					, {INTERFACE(ILogicPlugin), m_impl}
				},
				{
					// Commented code shows how to add new references. Uncomment and modify according to type used in plugin.h
					//	{INTERFACE(IExample), m_exampleReference}
				}
	);
}

Plugin::~Plugin()
{
}
